#include "Track.h"

Track::Track(float coef_roce , float acc  , float masa_vehiculo)
{
    this->coef_roce = coef_roce ; 
    this->acc = acc ;
    this->masa_vehiculo = masa_vehiculo;

    ir= 0 ; ic = 0 ;
}


void Track::addCurva(float radio, float len_ang) //len_angulo en grados sexagesimales
{
    Curva newCurva(coef_roce , radio , masa_vehiculo, len_ang) ;
    Vec_Curvas.push_back(newCurva) ;


    if (Vec_Rectas.size()>0)
    {
        //Al completar una curva,entregar su velocidad a la recta anterior
        Vec_Rectas[ic].finishRecta(newCurva.getSpeed()); 
    }

    ic +=1 ;

}

void Track::addRecta(float largo)
{
    float v_inicial;
    if (Vec_Curvas.size()>0)
    {
        v_inicial = Vec_Curvas[ir-1].getSpeed() ;
    }
    else //Situacion que esta sea la primera recta
    {
        v_inicial = 0 ;
    }
        
    Recta newRecta(v_inicial,largo,acc);
    Vec_Rectas.push_back(newRecta);
    ir+=1;
}

void Track::FinalizarTrack()
{
    Vec_Rectas[ir-1].finishRecta(Vec_Rectas[ir-1].getVInicial());
}

float Track::getAc()
{
    return acc;
}

float Track::getMasa()
{
    return masa_vehiculo;
}

void Track::setAc(float x)
{
   this->acc = x;
}
void Track::setMasa(float x)
{
   this->masa_vehiculo = x;
}
void Track::setCoef(float x)
{
   this->coef_roce = x;
}
