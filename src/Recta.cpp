#include "Recta.h"

Recta::Recta(float v_entrada , float largo, float acc){
    //Por como planeamos el desarrollo linea de la recta
    //El constructor dejara la pista incompleta
    this->v_entrada = v_entrada;
    this->largo = largo;
    this->acc = acc;
}

void Recta::finishRecta(float v_salida){
    //Aqui se completa la pista, cuando ya tengamos a que velocidad debe terminar
    this->v_salida = v_salida;
    makeTiempo(this->acc, this->v_entrada, this->v_salida, this->largo);
}

float Recta::getTiempo1(){
  return tiempo1;
}

float Recta::getTfinal(){
  return tiempo;
}

float Recta::getVmax() {
  return vmax;
}

float Recta::getVInicial(){
    return v_entrada;
}

float Recta::getVSalida()
{
    return v_salida;
}


void Recta::makeTiempo(int acc, int v0, int vf, int largo){

  int intentos = 10;

  if (v0>0)
  {
      tiempo1 = ((largo / v0) + (largo/vf)) / 2;
  }
  else
  {
      tiempo1= (largo/vf)/2;
  }
  int num = 2;

  while (intentos>0){

    int x1 = v0 * tiempo1 + 0.5 * acc * pow(tiempo1,2);

    if(x1 >largo){
      tiempo1 = tiempo1/2;
      continue;
    }

    vmax = v0 + acc*tiempo1;
    tiempo = ((vmax-vf)/acc) + tiempo1;
    float distancia = x1+(vmax*(tiempo-tiempo1))-(acc/2)* pow((tiempo-tiempo1),2);

    if (distancia < largo)
      tiempo1 = tiempo1+(tiempo1/num);

    else if (distancia > largo)
      tiempo1 = tiempo1-(tiempo1/num);

    else
      break;

    intentos-=1;
    num*=2;
  }
}

