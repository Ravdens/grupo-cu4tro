#include "Curva.h"
#include <cmath>

Curva::Curva(float k , float r , float m, float theta)
{  
    radio = r;
    maxSpeed = sqrt(k*G*r);
    fCentripeta = m*(pow(maxSpeed,2)/r);
    tiempo = (2*pi*radio*theta/360) / maxSpeed;
}

float Curva::getSpeed()
{return maxSpeed;}

float Curva::getCentripeta()
{return fCentripeta;}

float Curva::getRadio()
{return radio;}

float Curva::gettiempo()
{return tiempo;}
