#define G 9.807
#define pi 3.14

class Curva
{
public:
    Curva(float k , float r , float m, float theta);
    float getSpeed();
    float getCentripeta();
    float getRadio();
    float gettiempo();

private:
    float tiempo;
    float radio;
    float maxSpeed;
    float fCentripeta;
};
