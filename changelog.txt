Versión 1.0:
-Entrada y salida por consola
-Retorno de velocidad máxima y Fuerza centripeta

Versión 1.1:
-Agregadas nuevas funciones:
	getVelocidad(constante, radio)
	getFuerzaCentripeta(velocidad, masa, radio)

organizan el calculo de incognitas

Versión 1.2:
-Agregadas nuevas funciones:
	getPeso()
	getMasa()

Permiten uso de distintas unidades

-Renombre de funciones:
	getVelocidad(...)        -> Velocidad()
	getFuerzaCentripeta(...) -> FuerzaCentripeta()

Versión 1.3:
-Arreglados múltiples errores en transformación de unidades
-Agregado un makefile simple

Versión 2.0:
-Replanteamiento completo de la estructura interna
-Pasado de orientación lineal a orientación a objetos
-Cambiada orientación de una curva singular a múltiples curvas y rectas seguidas
-Agregados 6 nuevos archivos, 2 por clase

	Track.h
	Track.cpp
	Recta.h
	Recta.cpp
	Curva.h
	Curva.cpp

-Implimentado nuevo makefile para los archivos

Versión 2.1:
-Se agregó capacidad de usar gráficos

Version 3.0:
-Primera versión funcional de la interfaz grafica. Contiene el funcionamiento de nucleo y 1 forma default. 
